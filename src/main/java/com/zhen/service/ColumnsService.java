package com.zhen.service;

import java.util.List;

import com.zhen.pojo.Columns;

public interface ColumnsService {

	List<Columns> findColumnsBySchemaAndTableName(String tableSchema, String tableName);

}
