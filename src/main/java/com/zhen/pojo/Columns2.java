package com.zhen.pojo;

import java.util.Map;

import com.zhen.annotation.ProcessBean;

public class Columns2 {

	private String tableSchema;
	private String tableName;
	private String columnName;
	private String columnType;
	private String columnKey;
	private String columnComment;
	
	private Map<String, Object> process;
	
	
	public Map<String, Object> getProcess() {
		return process;
	}
	public void setProcess(Map<String, Object> process) {
		this.process = process;
	}
	public String getTableSchema() {
		return tableSchema;
	}
	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public String getColumnKey() {
		return columnKey;
	}
	public void setColumnKey(String columnKey) {
		this.columnKey = columnKey;
	}
	public String getColumnComment() {
		return columnComment;
	}
	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}
	
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
}
