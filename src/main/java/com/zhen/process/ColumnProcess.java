package com.zhen.process;

import java.text.ParseException;

import com.zhen.annotation.HtmlPolicy;
import com.zhen.annotation.Process;
import com.zhen.annotation.ProcessMethod;
import com.zhen.annotation.ProcessParam;
import com.zhen.annotation.TypePolicy;

@Process(type = TypePolicy.varchar)
public class ColumnProcess  extends BaseProcess{

	
	@ProcessMethod(name="列数据同步")
	public String column(@ProcessParam(name="选择相同值列",html=HtmlPolicy.select)String column) throws ParseException{
		
		return column;
	}	
	
	
}
