package com.zhen.util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ResourceBundle;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class CheckedUrlUtil {

	public static String[] STATUS;
	private static final Logger log = LoggerFactory.getLogger(CheckedUrlUtil.class);
	static {
		ResourceBundle bundle = ResourceBundle.getBundle("config/check");
		String str = bundle.getString("url.status");
		if (!StringUtils.isEmpty(str)) {
			STATUS = str.split(",");
		}
	}

	public static int testWsdlConnection(String address) throws Exception {
		return testWsdlConnection(address,1000);
	}

	public static void main(String[] args) {
		try {
			System.out.println(CheckedUrlUtil.testWsdlConnection("http://www.talkwallet.com/credit-cards/barclaycard-ring-mastercard.html?ref=uscc101"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int testWsdlConnection(String address, Integer connectTimeout) throws Exception {
		int status = 404;
		disableSSLCertCheck();
		URL urlObj = new URL(address);
		HttpURLConnection oc = (HttpURLConnection) urlObj.openConnection();
		oc.setUseCaches(false);
		oc.setConnectTimeout(10000); // 设置超时时间
		oc.setReadTimeout(10000);
		status = oc.getResponseCode();// 请求状态
		
		return status;
	}
	private static void disableSSLCertCheck() throws NoSuchAlgorithmException, KeyManagementException {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

	
}
