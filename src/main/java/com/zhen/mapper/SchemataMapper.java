package com.zhen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.zhen.pojo.Schemata;

@Mapper
public interface SchemataMapper {

	List<Schemata> findSchematas();
}
