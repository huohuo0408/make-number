package com.zhen.service;

import java.util.List;

import com.zhen.pojo.Tables;

public interface TablesService {

	List<Tables> findByTableSchema(String tableSchema);

	List<Tables> findTables();
}
