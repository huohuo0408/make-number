package com.zhen.process;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.zhen.annotation.HtmlPolicy;
import com.zhen.annotation.Process;
import com.zhen.annotation.ProcessMethod;
import com.zhen.annotation.ProcessParam;
import com.zhen.annotation.TypePolicy;
import com.zhen.util.StringUtil;
import com.zhen.util.key.IoergGeneratorKey;

@Process(type = TypePolicy.varchar)
public class StringProcess extends BaseProcess{

	/*@ProcessMethod(name="姓名")
	public String name(@ProcessParam(name="最大值",html=HtmlPolicy.number)int maxLength,//
			@ProcessParam(name="最小值",html=HtmlPolicy.number)int minLength){
		
		return "张三";
	}
	@ProcessMethod(name="性别")
	public String age(@ProcessParam(name="最小值",html=HtmlPolicy.number)int minLength){
		
		return "男";
	}*/
	
	@ProcessMethod(name="随机姓名")
	public String realName(@ProcessParam(name="默认值",html=HtmlPolicy.string)String name){
		
		if(StringUtils.isNotBlank(name)){
			return name;
		}
		return StringUtil.getName();
	}
	
	
}
