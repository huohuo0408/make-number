package com.zhen.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

	
	public static void getFiles(List<File> fileList, String path) {
        File[] allFiles = new File(path).listFiles();
        if(allFiles == null){
        	return;
        }
        for (int i = 0; i < allFiles.length; i++) {
            File file = allFiles[i];

            if (file.isFile()) {
                    fileList.add(file);
            } else  {
                getFiles(fileList, file.getAbsolutePath());
            }
        }
    }
	
	public static void main(String[] args) {
		
		
		List<File> list = new ArrayList<File>();
		FileUtils.getFiles(list, UrlUtil.getPath());
		System.out.println(list.get(0));
		System.out.println(list.get(0).getName());
		System.out.println(list.get(0).getPath());
	}
}
