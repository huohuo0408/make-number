package com.zhen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhen.mapper.ColumnsMapper;
import com.zhen.pojo.Columns;
import com.zhen.service.ColumnsService;

@Service
public class ColumnsServiceImpl implements ColumnsService{

	@Autowired
	private ColumnsMapper columnsMapper;
	
	@Override
	public List<Columns> findColumnsBySchemaAndTableName(String tableSchema, String tableName){
		return columnsMapper.findColumnsBySchemaAndTableName(tableSchema, tableName);
		
	}
}
