package com.zhen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.zhen.pojo.Tables;

@Mapper
public interface TablesMapper {

	List<Tables> findTablesBySchema(String schema);
	List<Tables> findTables();
}
