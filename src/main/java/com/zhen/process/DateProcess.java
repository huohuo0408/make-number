package com.zhen.process;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import com.zhen.annotation.HtmlPolicy;
import com.zhen.annotation.Process;
import com.zhen.annotation.ProcessMethod;
import com.zhen.annotation.ProcessParam;
import com.zhen.annotation.TypePolicy;
import com.zhen.util.DateUtils;

@Process(type = TypePolicy.varchar)
public class DateProcess extends BaseProcess{

	@ProcessMethod(name="时间戳到秒")
	public long datadatetime(@ProcessParam(name="开始时间(yyyy-MM-dd HH:mm:ss)",html=HtmlPolicy.string)String start,//
			@ProcessParam(name="间隔",html=HtmlPolicy.number)Integer jiange) throws ParseException{
		
		Date buildDate = DateUtils.buildDate(start,"yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		if(getPrevValue() != null){
			buildDate = new Date((Long)getPrevValue() * 1000L);
		}
		calendar.setTime(buildDate);
		if(getPrevValue() != null){
			calendar.add(calendar.MINUTE, jiange);
		}
		return calendar.getTimeInMillis() / 1000L;
	}
	
}
