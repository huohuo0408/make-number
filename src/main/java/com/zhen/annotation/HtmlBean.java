package com.zhen.annotation;

public class HtmlBean {

	private String name;
	private String html;
	private String type;
	private Object value;
	private String paramType;
	//自定义标签，处理器返回的html代码
	private String customize;
	
	
	public String getCustomize() {
		return customize;
	}
	public void setCustomize(String customize) {
		this.customize = customize;
	}
	public String getParamType() {
		return paramType;
	}
	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	public HtmlBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public HtmlBean(String name, String html, String type, String paramType,String customize) {
		super();
		this.name = name;
		this.html = html;
		this.type = type;
		this.paramType = paramType;
		this.customize = customize;
	}
	@Override
	public String toString() {
		return "HtmlBean [name=" + name + ", html=" + html + ", type=" + type + ", value=" + value + ", paramType="
				+ paramType + "]";
	}
	
	
	
}
