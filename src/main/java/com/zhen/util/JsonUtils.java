package com.zhen.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class JsonUtils {

	 public static boolean createDBJSONFile(String jsonData, String fileName,String dbName){
		 
		 String path = String.format("%s\\%s\\", UrlUtil.getPath(), dbName);
		 return createJsonFile(jsonData, path, fileName);
	 }
	 public static String getDBJSONFile(String path){
		 
		 return getDatafromFile(path);
	 }
	
    /**
     * 将JSON数据格式化并保存到文件中
     * @param jsonData 需要输出的json数
     * @param filePath 输出的文件地址
     * @return
     */
    public static boolean createJsonFile(String jsonData, String filePath, String fileName) {
    	
    	if(StringUtils.isBlank(jsonData)){
    		return false;
    	}
    	Object json = JSON.parse(jsonData);
        String content = JSON.toJSONString(json, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteDateUseDateFormat);
        // 标记文件生成是否成功
        boolean flag = true;
        // 生成json格式文件
        try {
            // 保证创建一个新文件
            File file = new File(filePath);
            if (!file.exists()) { // 如果父目录不存在，创建父目录
                file.mkdirs();
            }
            String path = String.format("%s\\%s", filePath,  fileName);
            file = new File(path);
            if (file.exists()) { // 如果已存在,删除旧文件
                file.delete();
            }
            file.createNewFile();
            // 将格式化后的字符串写入文件
            Writer write = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
            write.write(content);
            write.flush();
            write.close();
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 读取本地json文件
     * @Title: getDatafromFile   
     * @param: @param fileName
     * @param: @return      
     * @return: String      
     * @throws
     */
    private static String getDatafromFile(String fileName) {
        
        String Path=fileName;
           BufferedReader reader = null;
         String laststr = "";
           try {
             FileInputStream fileInputStream = new FileInputStream(Path);
              InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
               reader = new BufferedReader(inputStreamReader);
              String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                 laststr += tempString;
             }
              reader.close();
        } catch (IOException e) {
            e.printStackTrace();
           } finally {
              if (reader != null) {
                  try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                   }
              }
          }
          return laststr;
      }
    
   
    
    
    public static void main(String[] args) {
    	
//    	String json = "[{a:123,b:345},{d:456,c:789}]";
//    	JsonUtils.createDBJSONFile(json,"test","ioerg");
	}
}
