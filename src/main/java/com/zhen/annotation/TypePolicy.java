package com.zhen.annotation;

import java.math.BigDecimal;

public enum TypePolicy {

	
	varchar("varchar"),
	decimal("decimal");
	
	String code;

	private TypePolicy(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
