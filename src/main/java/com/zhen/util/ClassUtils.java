package com.zhen.util;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.druid.sql.visitor.functions.Char;

public class ClassUtils {

	
	public static Class<?> getBaseClass(String type) throws ClassNotFoundException{
		
		
		
		if("int".equals(type)){
			return int.class;
		}else if("long".equals(type)){
			return long.class;
		}else if("double".equals(type)){
			return double.class;
		}
		else if("byte".equals(type)){
			return byte.class;
		}
		else if("short".equals(type)){
			return short.class;
		}
		else if("boolean".equals(type)){
			return boolean.class;
		}
		else if("float".equals(type)){
			return float.class;
		}
		else if("char".equals(type)){
			return char.class;
		}
		return Class.forName(type);
	}
	public static Object parsClassVal(String type, Object val) throws ClassNotFoundException{
		
		
		if(String.class.getName().equals(type)){
			
			if(val == null){
				return "";
			}
			return String.valueOf((String)val);
		}
		else if("int".equals(type)||Integer.class.getName().equals(type)){
			
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return 0;
			}
			return Integer.valueOf((String)val);
		}else if("long".equals(type)||Long.class.getName().equals(type)){
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return 0l;
			}
			return Long.valueOf((String)val);
		}else if("double".equals(type)||Double.class.getName().equals(type)){
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return 0D;
			}
			return Double.valueOf((String)val);
		}
		else if("byte".equals(type)||Byte.class.getName().equals(type)){
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return (byte)0;
			}
			return Byte.valueOf((String)val);
		}
		else if("short".equals(type)||Short.class.getName().equals(type)){
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return (short)0;
			}
			return Short.valueOf((String)val);
		}
		else if("boolean".equals(type)||Boolean.class.getName().equals(type)){
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return false;
			}
			return Boolean.valueOf((String)val);
		}
		else if("float".equals(type)||Float.class.getName().equals(type)){
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return (float)0;
			}
			return Float.class;
		}
		else if("char".equals(type)||Char.class.getName().equals(type)){
			if(val == null || StringUtils.isBlank(String.valueOf(val))){
				return (char)' ';
			}
			return (Char)val;
		}
		return null;
	}
}
