package com.zhen.annotation;

import java.lang.reflect.Method;

import com.zhen.pojo.Columns;

public class ParamBean {

	private Object bean;
	private Object prevValue;
	private Columns column;
	private Method method;
	private Object[] param;
	
	
	public Object[] getParam() {
		return param;
	}
	public void setParam(Object[] param) {
		this.param = param;
	}
	public Object getBean() {
		return bean;
	}
	public void setBean(Object bean) {
		this.bean = bean;
	}
	public Object getPrevValue() {
		return prevValue;
	}
	public void setPrevValue(Object prevValue) {
		this.prevValue = prevValue;
	}
	public Columns getColumn() {
		return column;
	}
	public void setColumn(Columns column) {
		this.column = column;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	
	
}
