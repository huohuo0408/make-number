package com.zhen.mapper;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * Created by dell on 2019/4/8.
 */
@Mapper
public interface DataBatchInsertCustomMapper {
    public int insertByBatch(Map paramsMap);
}
