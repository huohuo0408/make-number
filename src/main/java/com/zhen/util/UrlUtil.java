package com.zhen.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

public class UrlUtil {

	private static String path = "";
	private static final Logger log = LoggerFactory.getLogger(UrlUtil.class);
	
	public static String getPath(){
		
		if(StringUtils.isNotBlank(path)){
			return path;
		}
		
		ResourceBundle bundle = ResourceBundle.getBundle("config/user");
		String str = bundle.getString("json.path");
		if (StringUtils.isNotBlank(str)) {
			path = str;
		}else{
			try {
				File file = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "json");
				path = file.getPath();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return path;
	}
	
}
