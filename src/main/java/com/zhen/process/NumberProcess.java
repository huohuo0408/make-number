package com.zhen.process;

import java.math.BigDecimal;
import java.util.Random;

import com.zhen.annotation.HtmlPolicy;
import com.zhen.annotation.Process;
import com.zhen.annotation.ProcessMethod;
import com.zhen.annotation.ProcessParam;
import com.zhen.annotation.TypePolicy;

@Process(type = TypePolicy.decimal)
public class NumberProcess extends BaseProcess  {

	
	@ProcessMethod(name="随机小数")
	public BigDecimal number(@ProcessParam(name="最大值",html=HtmlPolicy.number)Double max,
			@ProcessParam(name="最小值",html=HtmlPolicy.number)Double min){
		Double retu = min + ((max - min) * new Random().nextDouble());
		BigDecimal bigDecimal = new BigDecimal(retu);
		
		return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	@ProcessMethod(name="随机整数")
	public BigDecimal decimal(@ProcessParam(name="最大值",html=HtmlPolicy.number)Double max,
			@ProcessParam(name="最小值",html=HtmlPolicy.number)Double min){
		Double retu = min + ((max - min) * new Random().nextDouble());
		BigDecimal bigDecimal = new BigDecimal(retu);
		
		return bigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP);
	}
}
