package com.zhen.service;

import java.util.List;
import java.util.Map;

public interface DataBatchInsertCustomService {

	int insertByBatchReturnCountHistory(List<Map<String, Object>> updateItems, String dbName, String tableName, String[] exceptColArr)
			throws Exception;

	
}
