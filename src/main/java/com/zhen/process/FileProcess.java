package com.zhen.process;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.CaseInsensitiveMap;

import com.zhen.annotation.FileBean;
import com.zhen.annotation.HtmlPolicy;
import com.zhen.annotation.OrderPolicy;
import com.zhen.annotation.Process;
import com.zhen.annotation.ProcessMethod;
import com.zhen.annotation.ProcessParam;
import com.zhen.annotation.TypePolicy;
import com.zhen.pojo.Columns;
import com.zhen.util.FileUtils;
import com.zhen.util.UrlUtil;

@Process(type = TypePolicy.varchar)
public class FileProcess  extends BaseProcess{

	
	@ProcessMethod(name="关联插入")
	public String column(@ProcessParam(name="选择子表文件",html=HtmlPolicy.customize,process="selectFile")String file,
						@ProcessParam(name="对应列(输入对应表列名)",html=HtmlPolicy.string)String columns,
						@ProcessParam(name="对应数量",html=HtmlPolicy.number)int number) throws Exception{
		
		Columns currColumns2 = getCurrColumns();
		List<Columns> columnsList = currColumns2.getProcess().getChildColumn();
		try {
			
			int currIndex2 = getCurrIndex();
			if(currIndex2 % number == 1){
				List<CaseInsensitiveMap> packInsert = BaseProcess.packInsert(1, columnsList);
				setInsertList(packInsert);
				Object value  = null;
				for(Map<String, Object> table : packInsert){
					if(columnsList.get(0).getTableSchema().equals(table.get("dbName")) &&
							columnsList.get(0).getTableName().equals(table.get("tableName"))){
						value = table.get(columns);
						break;
					}
				}
				return String.valueOf(value);
			}else{
				return String.valueOf(getPrevValue());
			}
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		return "";
	}	
	@ProcessMethod(name="关联插入数据共用", order = OrderPolicy.object)
	public String column(@ProcessParam(name="选择相同值列",html=HtmlPolicy.select)String column,
			@ProcessParam(name="对应列(输入对应表列名)",html=HtmlPolicy.string)String columns) throws ParseException{
		
		List<CaseInsensitiveMap> allInsertList2 = getAllInsertList();
		String result = null;
		for(Map<String, Object> map : allInsertList2){
			if(map.get(column) != null && map.get(column).equals(getCurrObj().get(column))){
				if(map.get(columns) != null){
					
					result = (String)map.get(columns);
					break;
				}
			}
		}
		if(result == null){
			afreshExecute();
		}
		return result;
	}	
	@ProcessMethod(name="同步自身数据至关联表", order = OrderPolicy.object)
	public String column(@ProcessParam(name="选择子表文件",html=HtmlPolicy.customize,process="selectFile")String file,
						@ProcessParam(name="对应列(输入对应表列名)",html=HtmlPolicy.string)String columns,
						@ProcessParam(name="自身列(输入自身表列名)",html=HtmlPolicy.string)String columns1,
						@ProcessParam(name="获取列(输入对应表列名)",html=HtmlPolicy.string)String columns2,
						@ProcessParam(name="对应数量",html=HtmlPolicy.number)int number) throws Exception{
		
		Columns currColumns2 = getCurrColumns();
		
		List<Columns> columnsList = currColumns2.getProcess().getChildColumn();
		for(Columns col : columnsList){
			if(columns.equals(col.getColumnName())){
				col.setProcess(null);
			}
		}
		try {
			
			int currIndex2 = getCurrIndex();
			if(currIndex2 % number == 1){
				List<CaseInsensitiveMap> packInsert = BaseProcess.packInsert(1, columnsList);
				
				Object value  = null;
				for(Map<String, Object> table : packInsert){
					if(columnsList.get(0).getTableSchema().equals(table.get("dbName")) &&
							columnsList.get(0).getTableName().equals(table.get("tableName"))){
						value = table.get(columns2);
						if(getCurrObj().get(columns1) == null){
							//如果指定列还未执行，则设置重新执行
							afreshExecute();
							return null;
						}
						table.put(columns, getCurrObj().get(columns1));
						break;
					}
				}
				setInsertList(packInsert);
				return String.valueOf(value);
			}else{
				return String.valueOf(getPrevValue());
			}
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return "";
	}	
	
	/*@ProcessMethod(name="同步自身数据至关联表", order = OrderPolicy.object)
	public String column(@ProcessParam(name="选择子表文件",html=HtmlPolicy.customize,process="selectFile")String file,
						@ProcessParam(name="对应列(输入对应表列名)",html=HtmlPolicy.string)String columns,
						@ProcessParam(name="自身列(输入自身表列名)",html=HtmlPolicy.string)String columns1,
						@ProcessParam(name="获取列(输入对应表列名)",html=HtmlPolicy.string)String columns2,
						@ProcessParam(name="对应数量",html=HtmlPolicy.number)int number) throws Exception{
		
		Columns currColumns2 = getCurrColumns();
		
		List<Columns> columnsList = currColumns2.getProcess().getChildColumn();
		String[] columnsArray = columns.split(",");
		String[] columns1Array = columns1.split(",");
		for(Columns col : columnsList){
			for(String localColumns : columnsArray){
				if(localColumns.equals(col.getColumnName())){
					col.setProcess(null);
				}
			}
		}
		try {
			
			int currIndex2 = getCurrIndex();
			if(currIndex2 % number == 1){
				List<CaseInsensitiveMap> packInsert = BaseProcess.packInsert(1, columnsList);
				
				Object value  = null;
				local:for(Map<String, Object> table : packInsert){
					if(columnsList.get(0).getTableSchema().equals(table.get("dbName")) &&
							columnsList.get(0).getTableName().equals(table.get("tableName"))){
						value = table.get(columns2);
						
						int x = 0;
						for(String localColumns1 : columns1Array){
							
							if(getCurrObj().get(localColumns1) == null){
								//如果指定列还未执行，则设置重新执行
								afreshExecute();
								return null;
							}
							table.put(columnsArray[x], getCurrObj().get(localColumns1));
							x++;
							break local;
						}
					}
				}
				setInsertList(packInsert);
				return String.valueOf(value);
			}else{
				return String.valueOf(getPrevValue());
			}
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return "";
	}	*/
	public String selectFile(){
		
		
		List<File> list = new ArrayList<File>();
		FileUtils.getFiles(list, UrlUtil.getPath());
		
		List<FileBean> files = new ArrayList<FileBean>();
		StringBuilder str = new StringBuilder();
		str.append("<select v-model='html.value' data-live-search='true'>");
		for(File file : list){
			files.add(new FileBean(file.getName(),file.getPath().replace("\\", "/")));
			str.append("<option value='").append(file.getPath().replace("\\", "/")).append("'>").append(file.getName()).append("</option>");
		}
		str.append("<select>");
		return str.toString();
	}
}
