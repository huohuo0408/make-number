package com.zhen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhen.mapper.TablesMapper;
import com.zhen.pojo.Tables;
import com.zhen.service.TablesService;

@Service
public class TablesServiceImpl implements TablesService{

	@Autowired
	public TablesMapper tablesMapper;

	@Override
	public List<Tables> findByTableSchema(String tableSchema) {
		return tablesMapper.findTablesBySchema(tableSchema);
	}
	@Override
	public List<Tables> findTables(){
		return tablesMapper.findTables();
	}
}
