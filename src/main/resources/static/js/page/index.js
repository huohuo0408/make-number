var app;
(function($) {
	var backProcess = [];
	app = new Vue({
		el: '#app',
		data: {
			schematas:[],
			tables:[],
			columns:[],
			process:[],
			backProcess:[],
			selectTable:{},
			selectSchema:{},
			number:100,
			files: [],
			selectFile:'',
			selectTableName:'',
		},
		beforeMount() {},
		mounted() {
			
			var thiz = this;
			thiz.getProcess();
			thiz.getSchemas();
			//this.getTables();
			thiz.getFiles();
			
			
			var afterUrl =  window.location.search.substring(1);
			
			afterUrl = afterUrl.replace("filePath=","");
			afterUrl = decodeURIComponent(afterUrl);
			if(afterUrl != ''){
				
				$.post({
					url:"/freemarker/getConfigContent",
					dataType:"json",
					async: false,
					data:{url:afterUrl},
					success:function(data){
						thiz.selectFile = data.file;
						if(data != null){
							for(var x = 0; x< thiz.schematas.length; x++){
								if(thiz.schematas[x].schemaName == data.data[0].tableSchema){
									thiz.selectSchema = thiz.schematas[x];
									thiz.selectTableName = data.data[0].tableName;
									thiz.getTables();
									
									break;
								}
							}
							//thiz.selectTable = data.data[0];
						}
						thiz.columns = data.data;
						thiz.$nextTick(function(){
							
							var custom = $("p[htmltype='customize']");
							if(custom.length > 0){
								
								custom.each(function(a,b){
									
									var columnindex = $(b).attr("columnindex");
									var htmlindex = $(b).attr("htmlindex");
									var value = $(b).attr("value");
									var ele = $(b).find("select");
									if(ele.length == 0){
										
										ele = $(b).find("input");
									}
									if(ele.length == 0){
										
										ele = $(b).find("checkbox");
									}
									ele.val(value);
								})
							}
							$('select').selectpicker({
								noneSelectedText : '请选择',//默认显示内容
								liveSearch : true
							});
							$('select').selectpicker('refresh');
		                    $('select').selectpicker('render');
						})
						
						
					}
				})
			}
		},
		methods: {
			getFiles(){
				var thiz = this;
				$.get({
					url:"/freemarker/getConfig",
					dataType:"json",
					async: false,
					success:function(data){
						thiz.files = data;
					}
				})
			},
			selectFileFun(event){
				var ip = document.location.origin;
				var value = event.target.value;
				this.selectFile = value;
//				value = value.replic
				//escape(value)
				window.open(ip+"/html/index.html?filePath=" + value);
			},
			getProcess(){
				var thiz = this;
				$.get({
					url:"/freemarker/process",
					dataType:"json",
					async: false,
					success:function(data){
						thiz.process = data;
						backProcess = JSON.stringify(data);
					}
				})
			},
			getSchemas(){
				var thiz = this;
				$.get({
					url:"/freemarker/schematas",
					dataType:"json",
					async: false,
					success:function(data){
						thiz.schematas = data
						
					}
				})
			},
			getTables(){
				var thiz = this;
				$.get({
					url:"/freemarker/tables",
					dataType:"json",
					data: {
						dbName: thiz.selectSchema.schemaName
					},
					async: false,
					success:function(data){
						thiz.tables = data
						
						if(thiz.selectTableName){
							for(var x = 0; x< thiz.tables.length; x++){
								if(thiz.tables[x].tableName == thiz.selectTableName){
									thiz.selectTable = thiz.tables[x];
									thiz.selectTableName = "";
									break;
								}
							}
						}
						thiz.$nextTick(function(){
							/*$('select').selectpicker({
								noneSelectedText : '请选择',//默认显示内容
								liveSearch : true
							});*/
							$('#tableElment').selectpicker('refresh');
	                        $('#tableElment').selectpicker('render');
						})
                        
						//$('#tableElment').selectpicker('refresh');
					}
				})
			},
			select(){
				
				//var table = app.tables[app.selectTable];
				var thiz = this;
				$.get({
					url:"/freemarker/column",
					dataType: "json",
					data: {
						tableSchema: thiz.selectTable.tableSchema,
						tableName: thiz.selectTable.tableName
					},
					success: function(data){
						/*var process = JSON.parse(backProcess)[0];
						for(var x=0; x<data.length; x++){
							data[x].process = process;
						}*/
						app.columns = data;
						thiz.$nextTick(function(){
							/*$('select').selectpicker({
								noneSelectedText : '请选择',//默认显示内容
								liveSearch : true
							});*/
							$('select').selectpicker('refresh');
	                        $('select').selectpicker('render');
						})
					}
				})
				
			},
			selectSchemaFun(){
				var thiz = this;
				thiz.columns = [];
				app.getTables();
			},
			processSelect(event, index){
				var process = JSON.parse(backProcess)[event.target.value];
				$("#"+app.columns[index].columnName).find("select").selectpicker('destroy');;
				app.columns[index].process =process;
				
				app.$nextTick(function(){
					
					/*var custom = $("p[htmltype='customize']");
					if(custom.length > 0){
						
						custom.each(function(a,b){
							
							var columnindex = $(b).attr("columnindex");
							var htmlindex = $(b).attr("htmlindex");
							var ele = $(b).find("select");
							if(ele.length == 0){
								
								ele = $(b).find("input");
							}
							if(ele.length == 0){
								
								ele = $(b).find("checkbox");
							}
							var val = ele.val();
							thiz.columns[columnindex].process.html[htmlindex].value = val;
						})
					}*/
					
					$('select').selectpicker({
						noneSelectedText : '请选择',//默认显示内容
						liveSearch : true
					});
					$('select').selectpicker('refresh');
                    $('select').selectpicker('render');
				})
				/*var html = app.process[event.target.value].html;
				var htmlstr = "";
				for(var x = 0; x < html.length; x++){
					
					htmlstr += "<"+html[x].html+" type='"+html[x].type+"' v-model='columns["+index+"].process.html["+x+"].value'  class='form-control' placeholder='"+html[x].name+"'></"+html[x].html+">";
				}
				app.columns[index].html = htmlstr;
				this.$forceUpdate();
				$(".selectpicker" ).selectpicker('refresh');*/
			},
			save(){
				
				var thiz = this;
				var name = prompt("请输入文件名：默认文件名：【"+thiz.selectTable.tableName+"】默认会覆盖");
				
				if(name == null){
					return;
				}
				if(name == ""){
					name = thiz.selectTable.tableName;
				}
				var custom = $("p[htmltype='customize']");
				if(custom.length > 0){
					
					custom.each(function(a,b){
						
						var columnindex = $(b).attr("columnindex");
						var htmlindex = $(b).attr("htmlindex");
						var ele = $(b).find("select");
						if(ele.length == 0){
							
							ele = $(b).find("input");
						}
						if(ele.length == 0){
							
							ele = $(b).find("checkbox");
						}
						var val = ele.val();
						thiz.columns[columnindex].process.html[htmlindex].value = val;
					})
				}
				$.post({
					url:"/freemarker/save",
					dataType: "json",
					traditional:true,
					data: {
						columnsStr: JSON.stringify(thiz.columns),
						dbName: thiz.selectTable.tableSchema,
						tableName: name
					},
					success: function(data){
						if(data){
							alert("保存成功");
						}else{
							alert("保存失败");
						}
					}
				})
			},
			execute(){
				var thiz = this;
				if(thiz.number == ""){
					alert("请输入执行数量！");
					return;
				}
				
				var custom = $("p[htmltype='customize']");
				if(custom.length > 0){
					
					custom.each(function(a,b){
						
						var columnindex = $(b).attr("columnindex");
						var htmlindex = $(b).attr("htmlindex");
						var ele = $(b).find("select");
						if(ele.length == 0){
							
							ele = $(b).find("input");
						}
						if(ele.length == 0){
							
							ele = $(b).find("checkbox");
						}
						var val = ele.val();
						thiz.columns[columnindex].process.html[htmlindex].value = val;
					})
				}
				$.post({
					url:"/freemarker/execute",
					dataType: "json",
					traditional:true,
					data: {
						columnsStr: JSON.stringify(thiz.columns),
						dbName: thiz.selectTable.tableSchema,
						tableName: thiz.selectTable.tableName,
						number: thiz.number
					},
					success: function(data){
						if(data.code){
							alert("保存成功");
						}else{
							alert(data.msg);
						}
					}
				})
			}
		}
	})

})($);