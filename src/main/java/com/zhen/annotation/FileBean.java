package com.zhen.annotation;

public class FileBean {

	private String name;
	private String path;
	
	
	public FileBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FileBean(String name, String path) {
		super();
		this.name = name;
		this.path = path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
