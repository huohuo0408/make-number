package com.zhen.util;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 日期工具类
 * 
 * @ClassName: DateUtils
 * @author: huohz
 * @date: 2019年9月20日 下午12:02:49
 *
 */
public class DateUtils {
	/**
	 * 默认日期格式
	 */
	public static String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 格式化日期
	 *
	 * @param date
	 *            日期对象
	 * @return String 日期字符串
	 */
	public static String formatDate(Date date) {
		SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
		String sDate = f.format(date);
		return sDate;
	}

	/**
	 * 格式化日期
	 *
	 * @param date
	 *            日期对象
	 * @return String 日期字符串
	 */
	public static String formatDate(Date date, String format) {
		SimpleDateFormat f = new SimpleDateFormat(format);
		String sDate = f.format(date);
		return sDate;
	}

	/**
	 * 格式化日期
	 *
	 * @param date
	 *            日期对象
	 * @return String 日期字符串
	 * @throws ParseException
	 */
	public static Date buildDate(String date, String format) throws ParseException {
		if (date == null || "".equals(date)) {
			return null;
		}
		SimpleDateFormat f = new SimpleDateFormat(format);
		Date sDate = f.parse(date);
		return sDate;
	}

	public static Date buildDate(String date) throws ParseException {
		if (date == null || "".equals(date)) {
			return null;
		}
		SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
		Date sDate = f.parse(date);
		return sDate;
	}

	/**
	 * 前一年
	 *
	 * @param date
	 *            日期对象
	 * @return String 日期字符串
	 * @throws ParseException
	 */
	public static Date beforeYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTime(date);
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);

		return calendar.getTime();
	}

	/**
	 * 前一年
	 * 
	 * @param year
	 * @return
	 * @throws Exception
	 */
	public static String beforeYear(String year) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		// 下月一号
		Date date = sdf.parse(year);// 取时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.YEAR, -1);
		return sdf.format(calendar.getTime());
	}

	/**
	 * 下一年
	 *
	 * @param date
	 *            日期对象
	 * @return String 日期字符串
	 * @throws ParseException
	 */
	public static Date afterYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTime(date);
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);

		return calendar.getTime();
	}

	/**
	 * 获取某年第一天日期
	 *
	 * @param year
	 *            年份
	 * @return Date
	 */
	public static Date getCurrYearFirst(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, year);
		Date currYearFirst = calendar.getTime();
		return currYearFirst;
	}

	/**
	 * 获取某年最后一天日期
	 *
	 * @param year
	 *            年份
	 * @return Date
	 */
	public static Date getCurrYearLast(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, year);
		calendar.roll(Calendar.DAY_OF_YEAR, -1);
		Date currYearLast = calendar.getTime();
		return currYearLast;
	}

	/**
	 * 获取上月第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date beforeMonthFirstDay(Date date) {
		Calendar cal_1 = Calendar.getInstance();// 获取当前日期
		cal_1.setTime(date);
		cal_1.add(Calendar.MONTH, -1);
		cal_1.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
		return cal_1.getTime();
	}

	/**
	 * 获取当月第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date currMonthFirstDay(Date date) {
		Calendar cal_1 = Calendar.getInstance();// 获取当前日期
		cal_1.setTime(date);
		cal_1.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
		return cal_1.getTime();
	}

	/**
	 * 获取当月最后天
	 * 
	 * @param date
	 * @return
	 */
	public static Date currMonthLastDay(Date date) {
		Calendar cal_1 = Calendar.getInstance();// 获取当前日期
		cal_1.setTime(date);
		cal_1.set(Calendar.DAY_OF_MONTH, cal_1.getActualMaximum(Calendar.DAY_OF_MONTH));// 当前月最后一天
		return cal_1.getTime();
	}

	/**
	 * 获得指定日期的前一月
	 *
	 * @param date
	 * @return
	 * @throws Exception
	 */
	public static Date getMonthBefore(Date date) {// 可以用new
													// Date().toLocalString()传递参数
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int month = c.get(Calendar.MONTH);
		c.set(Calendar.MONTH, month - 1);
		return c.getTime();
	}

	/**
	 * 获得指定日期的后一月
	 *
	 * @param date
	 * @return
	 */
	public static Date getMonthAfter(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int month = c.get(Calendar.MONTH);
		c.set(Calendar.MONTH, month + 1);
		return c.getTime();
	}

	/**
	 * 获得指定日期的前一天
	 *
	 * @param date
	 * @return
	 * @throws Exception
	 */
	public static Date getSpecifiedDayBefore(Date date) {// 可以用new
															// Date().toLocalString()传递参数
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day - 1);
		return c.getTime();
	}

	/**
	 * 获得指定日期的后一天
	 *
	 * @param date
	 * @return
	 */
	public static Date getSpecifiedDayAfter(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day + 1);
		return c.getTime();
	}

	/**
	 * 根据指定日期加几天，或者减几天
	 *
	 * @param date
	 * @return
	 */
	public static Date getDayCaclNum(Date date, int num) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day + num);
		return c.getTime();
	}

	/**
	 * 获得指定时间的前一小时
	 *
	 * @param date
	 * @return
	 */
	public static Date getBeforeHourTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY) - 1);
		return c.getTime();
	}

	/**
	 * 计算两个日期之间相差的天数
	 * 
	 * @param smdate
	 *            较小的时间
	 * @param bdate
	 *            较大的时间
	 * @return 相差天数
	 * @throws ParseException
	 */
	public static int daysBetween(Date smdate, Date bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		smdate = sdf.parse(sdf.format(smdate));
		bdate = sdf.parse(sdf.format(bdate));
		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days))+1;
	}

	/**
	 * 获取月的天数
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static int getDaysNum(Date date) throws ParseException {
		return daysBetween(currMonthFirstDay(date), currMonthLastDay(date)) + 1;
	}

	/**
	 * 根据指定日期得到后一天
	 * 
	 * @param ymd
	 *            YYYYMMDD
	 * @return
	 * @throws Exception
	 */
	public static String getYesterDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// 下月一号
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DAY_OF_MONTH, -1);// 把日期往后增加一天
		date = calendar.getTime();
		String next = sdf.format(date);// 结束日期
		return next;
	}

	/**
	 * 根据指定日期得到后一天
	 * 
	 * @param ymd
	 *            YYYYMMDD
	 * @return
	 * @throws Exception
	 */
	public static String getNextDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// 下月一号
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DAY_OF_MONTH, 1);// 把日期往后增加一天
		date = calendar.getTime();
		String next = sdf.format(date);// 结束日期
		return next;
	}

	/**
	 * 根据指定日期得到下月第一天
	 * 
	 * @param ymd
	 * @return
	 * @throws Exception
	 */
	public static String getNextMonFristDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// 下月一号
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.MONTH, 1);// 把日期往后增加一天.整数往后推,负数往前移动
		date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
		String end = sdf.format(date);// 结束日期
		return end;
	}

	/**
	 * 根据指定日期得到上月第一天
	 * 
	 * @param ymd
	 * @return
	 * @throws Exception
	 */
	public static String getLastMonFristDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// 下月一号
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.MONTH, -1);// 把日期往后增加一天.整数往后推,负数往前移动
		date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
		String end = sdf.format(date);// 结束日期
		return end;
	}

	/**
	 * 根据指定日期得到上月最后一天
	 * 
	 * @param ymd
	 *            yyyyMMdd
	 * @return
	 * @throws Exception
	 */
	public static String getLastMonLastDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 0);
		String lastDay = sdf.format(calendar.getTime());// 本月最后第一天
		return lastDay;
	}

	/**
	 * 根据指定日期得到本月最后一天
	 * 
	 * @param ymd
	 *            yyyyMMdd
	 * @return
	 * @throws Exception
	 */
	public static String getMonLastDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String lastDay = sdf.format(calendar.getTime());// 本月最后第一天
		return lastDay;
	}

	/**
	 * 根据指定日期得到本月第一天
	 * 
	 * @param ymd
	 *            yyyyMMdd
	 * @return
	 * @throws Exception
	 */
	public static String getMonFirstDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		String lastDay = sdf.format(calendar.getTime());// 本月最后第一天
		return lastDay;
	}

	/**
	 * 根据指定日期得到次年第一天
	 * 
	 * @param ymd
	 *            yyyyMMdd
	 * @return
	 * @throws Exception
	 */
	public static String getNextYearOneDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, +1);
		String returnDate = sdf.format(calendar.getTime());// 次年第一天
		return returnDate;
	}

	/**
	 * 根据指定日期得到去年第一天
	 * 
	 * @param ymd
	 *            yyyyMMdd
	 * @return
	 * @throws Exception
	 */
	public static String getLastYearOneDay(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, -1);
		String returnDate = sdf.format(calendar.getTime());// 次年第一天
		return returnDate;
	}

	/**
	 * 字符串转日期
	 * 
	 * @param ymd
	 * @param format
	 *            日期格式串
	 * @return
	 * @throws Exception
	 */
	public static Date str2Date(String ymd, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = sdf.parse(ymd);// 取时间
		return date;
	}

	/**
	 * 获取去年同期时间
	 * 
	 * @param ymd
	 * @return
	 */
	public static String getLastYearday(String ymd) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, -1);
		return sdf.format(calendar.getTime());// 去年同期

	}

	/**
	 * 获取上月时间
	 * 
	 * @param ymd
	 * @return
	 */
	public static String getLastmon(String ymd) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(ymd);// 取时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		return sdf.format(calendar.getTime());// 上月

	}

	/**
	 * 根据指定周期获取上个时间点
	 * 
	 * @param dataDataeTime
	 * @param cycle
	 * @return
	 * @throws Exception
	 */
	public static String getPrvTimeByCycle(String dataDataeTime, String cycle) throws ParseException {
		Date dstr = buildDate(dataDataeTime, "yyyyMMddHHmm");
		Calendar cal = Calendar.getInstance();
		cal.setTime(dstr);
		if ("1".equals(cycle)) {
			cal.add(Calendar.MINUTE, -1);
		} else if ("2".equals(cycle)) {
			cal.add(Calendar.MINUTE, -5);
		} else if ("3".equals(cycle)) {
			cal.add(Calendar.MINUTE, -10);
		} else if ("4".equals(cycle)) {
			cal.add(Calendar.MINUTE, -15);
		} else if ("5".equals(cycle)) {
			cal.add(Calendar.MINUTE, -30);
		} else if ("6".equals(cycle)) {
			cal.add(Calendar.MINUTE, -60);
		} else if ("7".equals(cycle)) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		} else if ("8".equals(cycle)) {
			cal.add(Calendar.MONTH, -1);
		}
		return formatDate(cal.getTime(), "yyyyMMddHHmm");
	}

	/**
	 * 根据指定周期获取下个时间点
	 * 
	 * @param dataDataeTime
	 * @param cycle
	 * @return
	 * @throws Exception
	 */
	public static String getNextTimeByCycle(String dataDataeTime, String cycle) throws ParseException {
		Date dstr = buildDate(dataDataeTime, "yyyyMMddHHmm");
		Calendar cal = Calendar.getInstance();
		cal.setTime(dstr);
		if ("1".equals(cycle)) {
			cal.add(Calendar.MINUTE, 1);
		} else if ("2".equals(cycle)) {
			cal.add(Calendar.MINUTE, 5);
		} else if ("3".equals(cycle)) {
			cal.add(Calendar.MINUTE, 10);
		} else if ("4".equals(cycle)) {
			cal.add(Calendar.MINUTE, 15);
		} else if ("5".equals(cycle)) {
			cal.add(Calendar.MINUTE, 30);
		} else if ("6".equals(cycle)) {
			cal.add(Calendar.MINUTE, 60);
		} else if ("7".equals(cycle)) {
			cal.add(Calendar.DAY_OF_MONTH, 1);
		} else if ("8".equals(cycle)) {
			cal.add(Calendar.MONTH, 1);
		}
		return formatDate(cal.getTime(), "yyyyMMddHHmm");
	}

	/**
	 * 获取一天的开始时间 @Title: dayBeforeTime @param: @param
	 * date @param: @return @return: Long @throws
	 */
	public static Long dayBeforeTime(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Long time = calendar.getTimeInMillis() / 1000L;

		return time;
	}

	/**
	 * 获取一天的结束时间 @Title: dayEndTime @param: @param date @param: @return @return:
	 * Long @throws
	 */
	public static Long dayEndTime(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Long time = calendar.getTimeInMillis() / 1000L;

		return time;
	}

	/**
	 * 小时开始时间点
	 * @Title: hourBeforeTime   
	 * @param: @param date
	 * @param: @return      
	 * @return: Long      
	 * @throws
	 */
	public static Long hourBeforeTime(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Long time = calendar.getTimeInMillis() / 1000l;
		return time;
		
	}
	/**
	 * 小时结束时间点
	 * @Title: hourEndTime   
	 * @param: @param date
	 * @param: @return      
	 * @return: Long      
	 * @throws
	 */
	public static Long hourEndTime(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		Long time = calendar.getTimeInMillis() / 1000l;

		return time;
	}
	/**
	 * 获取每月最开始的时间点 @Title: monthBeforeTime @param: @param
	 * date @param: @return @return: Long @throws
	 */
	public static Long monthBeforeTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTimeInMillis() / 1000L;
	}

	/**
	 * 获取每月最后一个时间点 @Title: monthEndTime @param: @param
	 * date @param: @return @return: Long @throws
	 */
	public static Long monthEndTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, 1);
		calendar.roll(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTimeInMillis() / 1000L;
	}

	/**
	 * 每年开始的第一个时间点 @Title: yearBeforeTime @param: @param
	 * date @param: @return @return: Long @throws
	 */
	public static Long yearBeforeTime(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		return calendar.getTimeInMillis() / 1000L;

	}

	/**
	 * 每月结束时的最后一个时间点 @Title: yearEndTime @param: @param
	 * date @param: @return @return: Long @throws
	 */
	public static Long yearEndTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.DATE, 1);
		calendar.roll(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTimeInMillis() / 1000L;

	}

	public static String dataToStr(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/**
	 * 比较两个日期+时间大小 @Title: compareTo @param: @param sourceDate
	 * 比较对象 @param: @param targetDate 被比较对象 @param: @return @return: int
	 * (sourceDate > targetDate) = 1; (sourceDate = targetDate) = 0; (sourceDate
	 * < targetDate) = -1 @throws
	 */
	public static int compareTo(Date sourceDate, Date targetDate) {
		Calendar sourceCalendar = Calendar.getInstance();
		sourceCalendar.setTime(sourceDate);

		Calendar targetCalendar = Calendar.getInstance();
		targetCalendar.setTime(targetDate);
		return sourceDate.compareTo(targetDate);
	}

	/**
	 * 比较两个日期大小
	 * @Title: compareDateTo   
	 * @param: @param sourceDate 比较对象
	 * @param: @param targetDate 被比较对象
	 * @param: @return      
	 * @return: int  (sourceDate > targetDate) = 1; (sourceDate = targetDate) = 0; (sourceDate < targetDate) = -1
	 * @throws
	 */
	public static int compareDateTo(Date sourceDate, Date targetDate){
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	    String dateFirst = dateFormat.format(sourceDate);
	    String dateLast = dateFormat.format(targetDate);
	    int dateFirstIntVal = Integer.parseInt(dateFirst);
	    int dateLastIntVal = Integer.parseInt(dateLast);
	    if (dateFirstIntVal > dateLastIntVal) {
	        return 1;
	    } else if (dateFirstIntVal < dateLastIntVal) {
	        return -1;
	    }
	    return 0;
	}
	/**
	 * 将时间按转换成秒
	 *
	 * @param yyyyMMddHHmmss
	 *            传入标准格式字符创
	 * @return
	 */
	public static Long ymd2sec(String yyyyMMddHHmmss, String format) {
		Calendar ctime = Calendar.getInstance();
		if (StringUtils.isBlank(format)) {
			format = "yyyyMMddHHmmss";
		}
		SimpleDateFormat sFormat = new SimpleDateFormat(format);
		try {
			Date date = sFormat.parse(yyyyMMddHHmmss);
			ctime.setTime(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// 获得毫秒数据
		long dateMillis = ctime.getTimeInMillis();
		// 获得当前秒数
		long secs = dateMillis / 1000;
		// 返回秒数
		return secs;
	}

	public static Long ymd2sec(String yyyyMMddHHmmss) {
		// 字符创转化成时间
		Calendar ctime = strToCalendar(yyyyMMddHHmmss);
		// 获得毫秒数据
		long dateMillis = ctime.getTimeInMillis();
		// 获得当前秒数
		long secs = dateMillis / 1000;
		// 返回秒数
		return secs;
	}

	/**
	 * 字符串转换成日期时间
	 *
	 * @param time
	 *            字符创入参
	 * @return
	 */
	public static Calendar strToCalendar(String time) {
		Calendar ctime = Calendar.getInstance();
		DateFormat dformat = new SimpleDateFormat("yyyyMMddHHmmss");
		String zeros = "00000000000000";
		String destime = time;
		int timeLen = time.length();
		if (timeLen < 14) {
			destime = time.concat(zeros.substring(14 - timeLen));
		}
		try {
			ctime.setTime(dformat.parse(destime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ctime;
	}

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date str2Date = DateUtils.str2Date("2019-11-01", "yyyy-MM-dd");
		Date currMonthLastDay = DateUtils.currMonthLastDay(new Date());
		System.out.println(DateUtils.formatDate(str2Date, "yyyy-MM-dd HH:mm:ss"));
		System.out.println(DateUtils.formatDate(currMonthLastDay, "yyyy-MM-dd HH:mm:ss"));
		System.out.println(DateUtils.compareDateTo(str2Date, currMonthLastDay));

	}

}
