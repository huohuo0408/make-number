package com.zhen.annotation;

public enum HtmlPolicy {

	select("select", "select"),
	number("input", "number"),
	string("input", ""),
	//自定义类型
	customize("customize", "customize");
	
	String code;
	String type;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	private HtmlPolicy(String code, String type) {
		this.code = code;
		this.type = type;
	}
	
}
