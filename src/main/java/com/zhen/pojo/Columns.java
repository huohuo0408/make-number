package com.zhen.pojo;

import com.zhen.annotation.ProcessBean;

public class Columns {

	private String tableSchema;
	private String tableName;
	private String columnName;
	private String columnType;
	private String columnKey;
	private String columnComment;
	
	private ProcessBean process;
	
	
	
	public ProcessBean getProcess() {
		return process;
	}
	public void setProcess(ProcessBean process) {
		this.process = process;
	}
	public String getTableSchema() {
		return tableSchema;
	}
	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public String getColumnKey() {
		return columnKey;
	}
	public void setColumnKey(String columnKey) {
		this.columnKey = columnKey;
	}
	public String getColumnComment() {
		return columnComment;
	}
	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}
	
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	@Override
	public String toString() {
		return "Columns [tableSchema=" + tableSchema + ", tableName=" + tableName + ", columnName=" + columnName
				+ ", columnType=" + columnType + ", columnKey=" + columnKey + ", columnComment=" + columnComment
				+ ", process=" + process + "]";
	}
	
	
	
}
