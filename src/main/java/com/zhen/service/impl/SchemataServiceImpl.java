package com.zhen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhen.mapper.SchemataMapper;
import com.zhen.pojo.Schemata;
import com.zhen.service.SchemataService;

@Service
public class SchemataServiceImpl implements SchemataService{

	@Autowired
	private SchemataMapper schemataMapper;
	@Override
	public List<Schemata> findSchematas() {
		return schemataMapper.findSchematas();
	}

}
