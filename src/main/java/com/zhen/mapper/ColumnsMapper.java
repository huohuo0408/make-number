package com.zhen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zhen.pojo.Columns;

@Mapper
public interface ColumnsMapper {

	List<Columns> findColumnsBySchemaAndTableName(@Param("tableSchema")String tableSchema, @Param("tableName")String tableName);
	
}
