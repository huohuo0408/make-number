package com.zhen.annotation;

import java.util.ArrayList;
import java.util.List;

import com.zhen.pojo.Columns;

public class ProcessBean {

	private String name;
	private String beanName;
	private String methodName;
	private List<HtmlBean> html;
	
	private List<Columns> childColumn;
	
	
	public List<Columns> getChildColumn() {
		return childColumn;
	}
	public void setChildColumn(List<Columns> childColumn) {
		this.childColumn = childColumn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBeanName() {
		return beanName;
	}
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public List<HtmlBean> getHtml() {
		return html;
	}
	
	public void setHtml(List<HtmlBean> html) {
		this.html = html;
	}
	public void addHtml(HtmlBean policy){
		
		if(html == null){
			html = new ArrayList<HtmlBean>();
		}
		html.add(policy);
	}
	@Override
	public String toString() {
		return "ProcessBean [name=" + name + ", beanName=" + beanName + ", methodName=" + methodName + ", html=" + html
				+ "]";
	}
	
	
}
