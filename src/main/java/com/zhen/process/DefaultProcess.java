package com.zhen.process;

import org.apache.commons.lang3.StringUtils;

import com.zhen.annotation.HtmlPolicy;
import com.zhen.annotation.Process;
import com.zhen.annotation.ProcessMethod;
import com.zhen.annotation.ProcessParam;
import com.zhen.annotation.TypePolicy;

@Process(type = TypePolicy.varchar)
public class DefaultProcess extends BaseProcess {

	@ProcessMethod(name="默认处理器")
	public String name(@ProcessParam(name="默认值",html=HtmlPolicy.string)String vlue){
		
		if(StringUtils.isBlank(vlue)){
			return "";
		}
		return vlue;
	}
}
