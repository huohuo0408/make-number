package com.zhen.config;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@MapperScan(basePackages = "com.bjsxt.mapper.DataBatchInsertCustomMapper", sqlSessionFactoryRef = "test2SqlSessionFactory")
public class DataSourceOneConfig {

    @Bean(name = "one")
    @Qualifier("one")
    @ConfigurationProperties(prefix="spring.datasource.one")
    @Primary
    public DataSource getTwoDataSource(){
        return DataSourceBuilder.create().build();
    }
   /* @Bean(name = "one")
    @Qualifier("one")
    @ConfigurationProperties(prefix="spring.datasource.one")
    public DataSource getOneDataSource(){
    	return DataSourceBuilder.create().build();
    }*/
    
}
