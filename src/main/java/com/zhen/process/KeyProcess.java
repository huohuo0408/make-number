package com.zhen.process;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

import com.zhen.annotation.HtmlPolicy;
import com.zhen.annotation.Process;
import com.zhen.annotation.ProcessMethod;
import com.zhen.annotation.ProcessParam;
import com.zhen.annotation.TypePolicy;
import com.zhen.util.key.IoergGeneratorKey;

@Process(type = TypePolicy.varchar)
public class KeyProcess extends BaseProcess  {

	@ProcessMethod(name="18位分布式主键")
	public String key(@ProcessParam(name="默认值",html=HtmlPolicy.string)String key){
		
		if(StringUtils.isNotBlank(key)){
			return key;
		}
		return String.valueOf(IoergGeneratorKey.nextId());
	}
	@ProcessMethod(name="数字主键")
	public String indexKey(){
		
		if(getPrevValue() == null){
			return "0";
		}
		BigDecimal big = new BigDecimal((String)getPrevValue());
		big = big.add(new BigDecimal("1"));
		return String.valueOf(big.intValue());
	}
}
