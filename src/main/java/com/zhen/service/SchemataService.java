package com.zhen.service;

import java.util.List;

import com.zhen.pojo.Schemata;

public interface SchemataService {

	List<Schemata> findSchematas();
}
