package com.zhen.annotation;

public enum OrderPolicy {

	//生成列时执行
	column("column"),
	//生成完对象后执行
	object("object");
	
	String type;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	private OrderPolicy(String type) {
		this.type = type;
	}
	
}
